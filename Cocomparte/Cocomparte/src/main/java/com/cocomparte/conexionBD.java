package com.cocomparte;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class conexionBD {

    static final String linkBD = "jdbc:mysql://localhost/cocomparte?serverTimezone=UTC";
    static final String userBD = "backend";
    static final String passBD = "1234";
    protected static Connection conn = null;

    public static void connect(){
    
        if (conn == null) {
            try{
                conn = DriverManager.getConnection(linkBD, userBD, passBD);
                if (conn.isValid(0)) {
                    System.out.println("****************La connexió funciona****************");
                }
                else{
                    System.out.println("****************ERROR: La connexió NO funciona****************");
                }
            } catch (SQLException ex) {
                System.out.println("Conexion incorrecta");
                throw new ExceptionInInitializerError(ex);
            }
        }
    
    }
    
    public static Connection getConn(){
        return conn;
    }
    
    public static void close() throws SQLException{
        
        if (conn != null) {
            conn.close();
        }
        conn = null;
        
    }
}
