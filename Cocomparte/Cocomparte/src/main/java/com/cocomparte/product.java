package com.cocomparte;

import java.sql.Date;

import com.cocomparte.BD;

public class product {
    
    private String nombreProducto;
    private String origen;
    private int cantidad; //peso
    private int unidad; //cajas
    private boolean reservado;
    private int id_proveedor;
    private boolean preparado;
    private int categoria;
    private Date fechaCaducidad;


    product(){

    }

    public String getNombreProducto() {
        return this.nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    public String getOrigen() {
        return this.origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public int getCantidad() {
        return this.cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getUnidad() {
        return this.unidad;
    }

    public void setUnidad(int unidad) {
        this.unidad = unidad;
    }

    public boolean isReservado() {
        return this.reservado;
    }

    public boolean getReservado() {
        return this.reservado;
    }

    public void setReservado(boolean reservado) {
        this.reservado = reservado;
    }

    public int getId_proveedor() {
        return this.id_proveedor;
    }

    public void setId_proveedor(int id_proveedor) {
        this.id_proveedor = id_proveedor;
    }

    public boolean isPreparado() {
        return this.preparado;
    }

    public boolean getPreparado() {
        return this.preparado;
    }

    public void setPreparado(boolean preparado) {
        this.preparado = preparado;
    }

    public int getCategoria() {
        return this.categoria;
    }

    public void setCategoria(int categoria) {
        this.categoria = categoria;
    }

    public Date getFechaCaducidad() {
        return this.fechaCaducidad;
    }

    public void setFechaCaducidad(Date fechaCaducidad) {
        this.fechaCaducidad = fechaCaducidad;
    }

    public void conectarBD(){

    }

}
