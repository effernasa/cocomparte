package com.cocomparte;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;

import com.mysql.cj.xdevapi.Result;

public class BD {

    static final String linkBD = "jdbc:mysql://localhost/cocomparte?serverTimezone=UTC";
    static final String userBD = "backend";
    static final String passBD = "1234";
    protected static Connection conn = null;
    // ResultSet rs = null;

    public static void main(String[] args) {
        rellanarBD();
    }

    public static void rellanarBD() {

        try {
            conexionBD.connect();
            conn = conexionBD.getConn();
            /* Introducción de las ciudades */
            insertarTablaCiudad("Badalona");
            insertarTablaCiudad("Barberà del Vallès");
            insertarTablaCiudad("Barcelona");
            insertarTablaCiudad("Caldes de Montbui");
            insertarTablaCiudad("Canovelles del Vallès");
            insertarTablaCiudad("Castellar del Vallès");
            insertarTablaCiudad("Cornellà");
            insertarTablaCiudad("Granollers");
            insertarTablaCiudad("Hospitalet de Llobregat");
            insertarTablaCiudad("La Llagosta");
            insertarTablaCiudad("Les Franqueses del Vallès");
            insertarTablaCiudad("Lli\\çà d\\'Amunt");
            insertarTablaCiudad("Lli\\çà d\\'Avall");
            insertarTablaCiudad("Llinars del Vallès");
            insertarTablaCiudad("Manresa");
            insertarTablaCiudad("Martorell");
            insertarTablaCiudad("Martorelles");
            insertarTablaCiudad("Mataró");
            insertarTablaCiudad("Mollet del Vallès");
            insertarTablaCiudad("Montcada i Reixac");
            insertarTablaCiudad("Montmeló");
            insertarTablaCiudad("Palau de Plegamans i Solità");
            insertarTablaCiudad("Parets del Vallès");
            insertarTablaCiudad("Penedes de Mar");
            insertarTablaCiudad("Prat de Llobregat");
            insertarTablaCiudad("Terrassa");
            insertarTablaCiudad("Sabadell");
            insertarTablaCiudad("San Adrián de Besós");
            insertarTablaCiudad("Sant Cugat del Vallès");
            insertarTablaCiudad("Sant Quirze del Vallès");
            insertarTablaCiudad("Santa Coloma de Gramanet");
            insertarTablaCiudad("Santa Perpetua de Mogoda");
            insertarTablaCiudad("Vall d\\'Hebron");
            insertarTablaCiudad("Vilanova del Vallès");
            insertarTablaCiudad("Vilanova i la Geltrú");
            insertarTablaCiudad("Vilassar de Mar");

            /* Introducción de las categorias de los alimentos */
            insertarTablaCategoria("Cárnico");
            insertarTablaCategoria("Cereal y pan");
            insertarTablaCategoria("Frutas");
            insertarTablaCategoria("Frutos secos");
            insertarTablaCategoria("Lacteos");
            insertarTablaCategoria("Legumbres");
            insertarTablaCategoria("Pasta");
            insertarTablaCategoria("Tubérculo");
            insertarTablaCategoria("Verduras");
            insertarTablaCategoria("Pescado");

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void insertarTablaCiudad(String nombre) throws SQLException {

        try {
            conexionBD.connect();
            Connection conn = conexionBD.getConn();
            Statement ps = conn.createStatement();
            int count = ps.executeUpdate("INSERT INTO ciudad (Nombre) VALUES ('" + nombre + "')");
            System.out.println("Cantidad de ciudad insertado: " + count);
            conexionBD.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void insertarTablaCategoria(String nombre) throws SQLException {

        try {
            conexionBD.connect();
            Connection conn = conexionBD.getConn();
            Statement ps = conn.createStatement();
            int count = ps.executeUpdate("INSERT INTO categoria (Categoria) VALUES ('" + nombre + "')");
            System.out.println("Cantidad de categoria insertado: " + count);
            conexionBD.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void insertarTablaUsuario(String nombre, String apellidos, String dni, String telefono, String email,
            String password, String direccion, int idCiudad, int idUnidadFam) throws SQLException {

        try {
            conexionBD.connect();
            Connection conn = conexionBD.getConn();
            Statement ps = conn.createStatement();
            int count = ps.executeUpdate(
                    "INSERT INTO usuario (Nombre, Apellidos, DNI , Telefono, Email, Password, Unidad_Familiar, Direccion, Ciudad_id)"
                            + " VALUES ('" + nombre + "', '" + apellidos + "', '" + dni + "', '" + telefono + "', '"
                            + email + "', '" + password + "', +'" + idUnidadFam + "', '" + direccion + "', " + idCiudad
                            + ")");
            System.out.println("Cantidad de usuario insertado: " + count);
            conexionBD.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void insertarTablaProducto(String nombre, String origen, int cantidad, int unidades,
            String FechaCaducidad, int idProveedor, int preparado, int idCategoria)
            throws SQLException {

        try {
            conexionBD.connect();
            Connection conn = conexionBD.getConn();
            Statement ps = conn.createStatement();
            int count = ps.executeUpdate(
                "INSERT INTO productos (Nombre, Origen, Cantidad, Unidades, FechaCaducidad, Preparado, Categoria_idCategoria, Proveedor_id)"
                + " VALUES ('" + nombre + "', '" + origen + "', '" + cantidad + "', '" + unidades + "', '"
                + FechaCaducidad + "', '" + preparado + "', +'" + idCategoria + "'," + idProveedor + ")");
            System.out.println("Cantidad de producto insertado: " + count);
            conexionBD.close();
            // visualizarTablaProducto();
        } catch (Exception e) {
            e.printStackTrace();
            e.getMessage();
            e.getCause();
        }

    }

    public static ResultSet visualizarTablaUsuario(int id) throws SQLException {

        ResultSet rs = consulta("select * from usuario where id = " + id);

        try {

            System.out.println("--Usuario-- \nNombre: " + rs.getString("Nombre") + " | \nApellidos: "
                    + rs.getString("Apellidos") + " | \nD.N.I: " + rs.getString("DNI") + " | \nTelefono: "
                    + rs.getInt("Telefono") + " | \nEmail: " + rs.getString("email") + " | \nDirección: "
                    + rs.getString("Direccion") + " | \nCiudad_id: " + rs.getInt("Ciudad_id"));

        } catch (Exception e) {
            e.printStackTrace();
        }
        return rs;
    }

    public static ResultSet infoUsuario(int id) throws SQLException {

        ResultSet rs = consulta(
                "select u.id, u.Nombre, u.Apellidos, u.DNI, u.Telefono, u.Email, u.Direccion, c.Nombre from usuario u inner join ciudad c on u.Ciudad_id = c.id where u.id = " + id);

        try {
            

        } catch (Exception e) {
            e.printStackTrace();
        }
        return rs;

    }

    public static ResultSet infoProducto(int id) throws SQLException {

        ResultSet rs = consulta(
                "select p.Nombre, p.Origen, p.Cantidad, p.Unidades, p.FechaCaducidad, p.Preparado, c.Categoria from productos p inner join categoria c on p.Categoria_idCategoria = c.idCategoria where c.idCategoria = p.Categoria_idCategoria and p.Proveedor_id = "
                        + id);

        try {

            /*System.out.println("--Producto-- \nNombre: " + rs.getString("p.Nombre") + " | \nOrigen: " + rs.getString("Origen")
            + " |  \nCantidad: ");*/

        } catch (Exception e) {
            e.printStackTrace();
        }

        return rs;

    }

    public static ArrayList<String> visualizarCiudades() throws SQLException {
        ArrayList<String> ciudad = new ArrayList<String>();
        ResultSet results = consulta("select * from ciudad");
        try {
            while (results.next()) {

                // aca tienes que crear un BEAN NIVELES
                ciudad.add(results.getString("Nombre"));
                // y así sucesivamente con todos los campos del
                // bean VerCiudades
            }
        } catch (Exception e) {
            e.printStackTrace();
            e.getMessage();
            e.getCause();
        }
        return ciudad;
    }

    public static ArrayList<String> visualizarCategoria() throws SQLException {
        ArrayList<String> categoria = new ArrayList<String>();
        ResultSet results = consulta("select * from categoria");
        try {
            while (results.next()) {

                // aca tienes que crear un BEAN NIVELES
                categoria.add(results.getString("Categoria"));
                // y así sucesivamente con todos los campos del
                // bean VerCiudades
            }
        } catch (Exception e) {
            e.printStackTrace();
            e.getMessage();
            e.getCause();
        }
        return categoria;
    }

    public static ResultSet visualizarTablaProducto() throws SQLException {

        ResultSet rs = consulta("select * from productos");

        try {
            while (rs.next()) {
                System.out.println("--Producto-- \nNombre: " + rs.getString("Nombre") + " | \nOrigen: "
                        + rs.getString("Origen") + " | \nCantidad: " + rs.getInt("Cantidad") + " | \nUnidades"
                        + rs.getInt("Unidades") + " | \nFechaCaducidad: " + rs.getDate("FechaCaducidad")
                        + " | \nReservado: " + rs.getBoolean("Reservado") + " | \nProveedor_id: "
                        + rs.getInt("Proveedor_id") + " | \nPreparado: " + rs.getBoolean("Preparado")
                        + " | \nCategoria_idCategoria: " + rs.getInt("Categoria_idCategoria"));
            }
            conexionBD.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return rs;

    }

    /*
     * public String logearse(String email, String password) throws SQLException {
     * 
     * String nombre = null; try { ResultSet rs = consulta(
     * "select Nombre from usuario where email = '" + email + "' AND password='" +
     * password + "'"); if (rs.wasNull()) {
     * System.out.println("Este usuario no exisiste"); } else { nombre =
     * rs.getString("Nombre"); } } catch (Exception e) { e.printStackTrace(); }
     * return nombre;
     * 
     * }
     */

    public static String Nombre(int id) throws SQLException {

        String nombre = null;
        ResultSet rs = consulta("select Nombre from usuario where id =" + id);
        try {
            while (rs.next()) {
                nombre = rs.getString("Nombre");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return nombre;
    }

    public static void insertarUsuarioProveedor(int user)throws SQLException{

        try {
            conexionBD.connect();
            Connection conn = conexionBD.getConn();
            Statement ps = conn.createStatement();
            ps.executeUpdate("INSERT INTO proveedor (Usuario_id) VALUES ("+ user +")");

            // visualizarTablaProducto();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static ResultSet consulta(String query) throws SQLException {

        try {
            conexionBD.connect();
            Connection conn = conexionBD.getConn();
            Statement ps = conn.createStatement();
            return ps.executeQuery(query);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    public static void insertarUsuarioProducto(String user, String provedorid)
            throws SQLException {

        try {
            conexionBD.connect();
            Connection conn = conexionBD.getConn();
            Statement ps = conn.createStatement();
            int count = ps.executeUpdate(
                "INSERT INTO usuario_has_productos (Usuario_id,Productos_id)"
                + " VALUES ('" + user + "'," + provedorid + ")");
            System.out.println("Cantidad de producto insertado: " + count);
            conexionBD.close();
            // visualizarTablaProducto();
        } catch (Exception e) {
            e.printStackTrace();
            e.getMessage();
            e.getCause();
        }

    }
}