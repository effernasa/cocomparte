package com.cocomparte;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import com.google.protobuf.Descriptors.FileDescriptor.InternalDescriptorAssigner;

public class proveedor{

    private ArrayList<String> arrNomProve = new ArrayList<String>();
    private ArrayList<String> arrApellidos = new ArrayList<String>();
    private ArrayList<String> arrEmail = new ArrayList<String>();
    private ArrayList<String> arrEncuentro = new ArrayList<String>();
    private ArrayList<Integer> arrValoracion = new ArrayList<Integer>();



    public void datosProveedor(int proveedor_id) {
    
        try{
            ResultSet rs = BD.consulta("SELECT po.Valoracion,po.PuntoEncuentroPredeterminado,u.Nombre,u.Apellidos,u.Email FROM proveedor po INNER JOIN usuario u ON po.Usuario_id = u.id WHERE po.id = "+proveedor_id);
            while(rs.next()){     
                arrNomProve.add(rs.getString("Nombre"));
                arrApellidos.add(rs.getString("Apellidos"));
                arrEmail.add(rs.getString("Email"));
                arrEncuentro.add(rs.getString(2));
                arrValoracion.add(rs.getInt("Valoracion"));
            }
        }
        catch(SQLException e){
            e.printStackTrace();;
            e.getMessage();
            e.getSQLState();
            e.getErrorCode();
            e.getCause();
        }
    }

    public ArrayList<String> getArrNomProve(){
        return this.arrNomProve;
    }
    public void setArrNomProve(ArrayList<String> arrNomProve){
        this.arrNomProve = arrNomProve;
    }

    public ArrayList<String> getArrApellidos(){
        return this.arrApellidos;
    }
    public void setArrApellidos(ArrayList<String> arrApellidos){
        this.arrApellidos = arrApellidos;
    }

    public ArrayList<String> getArrEmail(){
        return this.arrEmail;
    }
    public void setArrEmail(ArrayList<String> arrEmail){
        this.arrEmail = arrEmail;
    }

    public ArrayList<String> getArrEncuentro(){
        return this.arrEncuentro;
    }
    public void setArrEncuentro(ArrayList<String> arrEncuentro){
        this.arrEncuentro = arrEncuentro;
    }

    public ArrayList<Integer> getArrValoracion(){
        return this.arrValoracion;
    }
    public void setArrValoracion(ArrayList<Integer> arrValoracion){
        this.arrValoracion = arrValoracion;
    }
}
