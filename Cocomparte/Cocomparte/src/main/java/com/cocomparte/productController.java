package com.cocomparte;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class productController {
    //ArrayList<String> listOfBanks = new ArrayList<>()
    private ArrayList<Integer> arrIdProductos = new ArrayList<Integer>();
    private ArrayList<String> arrProductos = new ArrayList<String>();
    private ArrayList<String> arrOrigen  = new ArrayList<String>();
    private ArrayList<Integer> arrCantidad = new ArrayList<Integer>();
    private ArrayList<Integer> arrUnidades = new ArrayList<Integer>();
    private ArrayList<String> arrFechaCad = new ArrayList<String>();
    private ArrayList<Boolean> arrPreparado = new ArrayList<Boolean>();

    private ArrayList<Integer> arrIntCategoria = new ArrayList<Integer>();
    private ArrayList<String> arrStringCategoria = new ArrayList<String>(){{
        add("img/img1.jpg");
        add("img/carne.jpg");
        add("img/pan.jpg");
        add("img/frutas.jpg");
        add("img/frutossecos.jpg");
        add("img/lacteos.jpg");
        add("img/legumbres.jpg");
        add("img/pasta.jpg");
        add("img/tuberculos.jpg");
        add("img/verduras.jpg");
        add("img/pescado.jpg");
    }};


    public void insertarProductosEnArray() {
        
        int contador = 0;
        try {
            ResultSet rs = BD.consulta("SELECT * FROM productos");
            while (rs.next()){
                arrIdProductos.add(rs.getInt("id"));
                arrProductos.add(rs.getString("Nombre"));
                arrOrigen.add(rs.getString("Origen"));
                arrCantidad.add(rs.getInt("Cantidad"));
                arrUnidades.add(rs.getInt("Unidades"));
                arrIntCategoria.add(rs.getInt("Categoria_idCategoria"));
                arrFechaCad.add(rs.getString("FechaCaducidad"));

                contador++;
                if(rs.getInt("Preparado") == 1){
                    arrPreparado.add(true);
                }else{
                    arrPreparado.add(false);
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
            e.getMessage();
            e.getSQLState();
            e.getErrorCode();
            e.getCause();
        }
    }
    


    public ArrayList<Integer> getArrIdProductos() {
        return this.arrIdProductos;
    }

    public void setArrIdProductos(ArrayList<Integer> arrIdProductos) {
        this.arrIdProductos = arrIdProductos;
    }

    public ArrayList<String> getArrProductos() {
        return this.arrProductos;
    }

    public void setArrProductos(ArrayList<String> arrProductos) {
        this.arrProductos = arrProductos;
    }

    public ArrayList<String> getArrOrigen() {
        return this.arrOrigen;
    }

    public void setArrOrigen(ArrayList<String> arrOrigen) {
        this.arrOrigen = arrOrigen;
    }

    public ArrayList<Integer> getArrCantidad() {
        return this.arrCantidad;
    }

    public void setArrCantidad(ArrayList<Integer> arrCantidad) {
        this.arrCantidad = arrCantidad;
    }

    public ArrayList<Integer> getArrUnidades() {
        return this.arrUnidades;
    }

    public void setArrUnidades(ArrayList<Integer> arrUnidades) {
        this.arrUnidades = arrUnidades;
    }

    public ArrayList<String> getArrFechaCad() {
        return this.arrFechaCad;
    }

    public void setArrFechaCad(ArrayList<String> arrFechaCad) {
        this.arrFechaCad = arrFechaCad;
    }

    public ArrayList<Boolean> getArrPreparado() {
        return this.arrPreparado;
    }

    public void setArrPreparado(ArrayList<Boolean> arrPreparado) {
        this.arrPreparado = arrPreparado;
    }

    public ArrayList<Integer> getArrIntCategoria() {
        return this.arrIntCategoria;
    }

    public void setArrIntCategoria(ArrayList<Integer> arrIntCategoria) {
        this.arrIntCategoria = arrIntCategoria;
    }

    public ArrayList<String> getArrStringCategoria() {
        return this.arrStringCategoria;
    }

    public void setArrStringCategoria(ArrayList<String> arrStringCategoria) {
        this.arrStringCategoria = arrStringCategoria;
    }
  

}
