package com.cocomparte;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.google.protobuf.Descriptors.FileDescriptor.InternalDescriptorAssigner;


public class myproductController {
    private ArrayList<String> arrNomUsuario = new ArrayList<String>();
    private ArrayList<String> arrApellidos = new ArrayList<String>();
    private ArrayList<Integer> arrIdProducto = new ArrayList<Integer>();
    private ArrayList<String> arrNomProducto = new ArrayList<String>();
    private ArrayList<String> arrOrigen = new ArrayList<String>();
    private ArrayList<String> arrDescripcion = new ArrayList<String>();
    private ArrayList<Integer> arrCantidad = new ArrayList<Integer>();
    private ArrayList<Integer> arrUnidades = new ArrayList<Integer>();
    private ArrayList<String> arrFecha = new ArrayList<String>();
    private ArrayList<Boolean> arrReservado = new ArrayList<Boolean>();
    private ArrayList<Boolean> arrPreparado = new ArrayList<Boolean>();
    private ArrayList<Integer> arrCategoriaId = new ArrayList<Integer>();
    private ArrayList<Integer> arrProveedorId = new ArrayList<Integer>();
    private ArrayList<Integer> arrUsuarioId = new ArrayList<Integer>();
    private ArrayList<String> arrFotoCategoria = new ArrayList<String>(){{
        add("img/img1.jpg");
        add("img/carne.jpg");
        add("img/pan.jpg");
        add("img/frutas.jpg");
        add("img/frutossecos.jpg");
        add("img/lacteos.jpg");
        add("img/legumbres.jpg");
        add("img/pasta.jpg");
        add("img/tuberculos.jpg");
        add("img/verduras.jpg");
        add("img/pescado.jpg");
    }};
    
    public void insertarMisProductos(int user_id) {
    
        try{
            ResultSet rs = BD.consulta("SELECT u.id,u.Nombre, u.Apellidos,p.id,p.Nombre,p.Origen,p.Descripcion,p.Cantidad,p.Unidades, p.FechaCaducidad, p.Reservado, p.Proveedor_id, p.Preparado, p.Categoria_idCategoria FROM usuario u INNER JOIN productos p JOIN usuario_has_productos up ON u.id= up.Usuario_id AND p.id = up.Productos_id WHERE u.id ="+user_id);
            while(rs.next()){     
                arrNomUsuario.add(rs.getString("Nombre"));
                arrApellidos.add(rs.getString("Apellidos"));
                arrIdProducto.add(rs.getInt(4));
                arrNomProducto.add(rs.getString(5));
                arrOrigen.add(rs.getString("Origen"));
                arrCantidad.add(rs.getInt("Cantidad"));
                arrUnidades.add(rs.getInt("Unidades"));
                arrFecha.add(rs.getString("FechaCaducidad"));
                arrReservado.add(rs.getBoolean("Reservado"));
                arrProveedorId.add(rs.getInt("Proveedor_id")); 
                arrUsuarioId.add(rs.getInt(1)); 
                arrCategoriaId.add(rs.getInt("Categoria_idCategoria"));

                if(rs.getInt("Preparado") == 1){
                    arrPreparado.add(true);
                }else{
                    arrPreparado.add(false);
                }
            }
        }
        catch(SQLException e){
            e.printStackTrace();;
            e.getMessage();
            e.getSQLState();
            e.getErrorCode();
            e.getCause();
        }
    }

    public ArrayList<String> getArrNomUsuario(){
        return this.arrNomUsuario;
    }
    public void setArrNomUsuario(ArrayList<String> arrNomUsuario){
        this.arrNomUsuario = arrNomUsuario ;
    }

    public ArrayList<String> getArrApellido(){
        return this.arrNomUsuario;
    }
    public void setArrApellido(ArrayList<String> arrApellidos){
        this.arrApellidos= arrApellidos ;
    }

    public ArrayList<Integer> getArrIdProducto(){
        return this.arrIdProducto;
    }
    public void setArrIdProducto(ArrayList<Integer> arrIdProducto ){
        this.arrIdProducto = arrIdProducto ;
    }

    public ArrayList<String> getArrNomProducto(){
        return this.arrNomProducto;
    }
    public void setArrNomProducto(ArrayList<String> arrNomProducto ){
        this.arrNomProducto = arrNomProducto ;
    }

    public ArrayList<String> getArrOrigen(){
        return this.arrOrigen;
    }
    public void setArrOrigen(ArrayList<String> arrOrigen){
        this.arrOrigen = arrOrigen ;
    }

    public ArrayList<String> getArrDescripcion(){
        return this.arrDescripcion;
    }
    public void setArrDescripcion(ArrayList<String> arrDescripcion){
        this.arrDescripcion = arrDescripcion ;
    }

    public ArrayList<Integer> getArrCantidad(){
        return this.arrCantidad;
    }
    public void setArrCantidad(ArrayList<Integer> arrCantidad){
        this.arrCantidad = arrCantidad ;
    }

    public ArrayList<Integer> getArrUnidades(){
        return this.arrUnidades;
    }
    public void setArrUnidades(ArrayList<Integer> arrUnidades){
        this.arrUnidades = arrUnidades ;
    }

    public ArrayList<String> getArrFecha(){
        return this.arrFecha;
    }
    public void setArrFecha(ArrayList<String> arrFecha){
        this.arrFecha = arrFecha;
    }

    public ArrayList<Boolean> getArrReservado(){
        return this.arrReservado;
    }
    public void setArrReservado(ArrayList<Boolean> arrReservado){
        this.arrReservado = arrReservado;
    }

    public ArrayList<Boolean> getArrPreparado(){
        return this.arrPreparado;
    }
    public void setArrPreparado(ArrayList<Boolean> arrPreparado){
        this.arrPreparado = arrPreparado;
    }

    public ArrayList<Integer> getArrPoductosId(){
        return this.arrProveedorId;
    }
    public void setArrPorductosId(ArrayList<Integer> arrProveedorId){
        this.arrProveedorId = arrProveedorId;
    }

    public ArrayList<Integer> getArrProveedorId(){
        return this.arrProveedorId;
    }
    public void setArrProveedorId(ArrayList<Integer> arrProveedorId){
        this.arrProveedorId = arrProveedorId;
    }

    public ArrayList<Integer> getArrUsuarioId(){
        return this.arrUsuarioId;
    }
    public void setArrProductosId(ArrayList<Integer> arrUsuarioId){
        this.arrUsuarioId = arrUsuarioId;
    }

    public ArrayList<Integer> getArrCategoriaId() {
        return this.arrCategoriaId;
    }
    public void setArrCategoriaId(ArrayList<Integer> arrCategoriaId) {
        this.arrCategoriaId = arrCategoriaId;
    }

    public ArrayList<String> getArrFotoCategoria() {
        return this.arrFotoCategoria;
    }
    public void setArrFotoCategoria(ArrayList<String> arrFotoCategoria) {
        this.arrFotoCategoria = arrFotoCategoria;
    }
}
