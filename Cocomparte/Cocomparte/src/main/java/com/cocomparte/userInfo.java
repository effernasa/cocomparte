package com.cocomparte;

import com.cocomparte.BD;
import java.sql.*;

public class userInfo {
    
public void main(String[] args) {
    //obtenerInformacionUsuario("hola");

}

    private int id;
    private String name;
    private String apellidos;
    private String dni;
    private int telefono;
    private String email;
    private String direccion;
    private int ciudad;
    private int unidadFamiliar;

    BD connBD = new BD();

    public userInfo(){

    }

    public void setId(int idString) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getDni() {
        return dni;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setUnidadFamiliar(int unidadFamiliar) {
        this.unidadFamiliar = unidadFamiliar;
    }

    public int getUnidadFamiliar() {
        return unidadFamiliar;
    }

    public void setCiudad(int ciudad) {
        this.ciudad = ciudad;
    }

    public int getCiudad() {
        return ciudad;
    }

    /*public void obtenerInformacionUsuario(String name){

        try {
            ResultSet informacionUsuario = connBD.visualizarTablaUsuario(name);

            name = informacionUsuario.getString("Nombre");
            apellidos = informacionUsuario.getString("Apellidos");
            dni = informacionUsuario.getString("D.N.I");
            telefono = informacionUsuario.getInt("Telefono");
            email = informacionUsuario.getString("Email");
            direccion = informacionUsuario.getString("Dirección");
            ciudad = informacionUsuario.getInt("Ciudad_id");
        } catch (Exception e) {
            e.printStackTrace();
        }


    }*/

}
