<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@page import="java.sql.*,java.util.*"%>
<%@page import="com.cocomparte.*"%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Solicitar producto</title>
    <link rel="icon" href="img/logo.png" type="image/x-icon">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
      integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <link href="styles/header.css" rel="stylesheet">
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="styles/carousel.css" rel="stylesheet">
</head>
<body>
    
<!--HEADER-->
<jsp:include page="header.jsp" />

<%String idproveedor=request.getParameter("idproveedor");
  System.out.println(idproveedor);
  String nombre = "";
  String apellido = "";
  String mail = "";
  int valoracion = 0;
  String lugar = "";
  String telefo = "";

  
  ResultSet infoProveedor = BD.consulta("SELECT po.Valoracion,po.PuntoEncuentroPredeterminado,u.Nombre,u.Apellidos,u.Email,u.Telefono FROM proveedor po INNER JOIN usuario u ON po.Usuario_id = u.id WHERE po.id = "+idproveedor+";");
  while(infoProveedor.next()){
    nombre = infoProveedor.getString("Nombre");
    apellido = infoProveedor.getString("Apellidos");
    mail = infoProveedor.getString("Email");
    lugar = infoProveedor.getString("PuntoEncuentroPredeterminado");
    valoracion = infoProveedor.getInt("Valoracion");
    telefo = infoProveedor.getString("Telefono");
  }

    System.out.println(nombre);
    System.out.println(apellido);
    System.out.println(mail);
    System.out.println(lugar);
    System.out.println(valoracion);
    System.out.println(telefo);

%>
<div class="row">
<div class="col-2"></div>
<div class="col-8 text-center">
<main role="main">
</br>
<h1 id="bloque1" style=position:relative;top:0.7rem;>Pagina de informaci&oacute;n del proveedor</br><h1 style=color:white;position:relative;top:0.8rem;><i> <%out.print(nombre +"  "+ apellido);%></i></h1></h1>
<div style=position:relative;top:4rem;>
<h3>Informaci&oacute; de contacto: </h3></br>
<p>Email: <b><%out.print(mail);%></b></p>
<p>Telefono: <b><%out.print(telefo);%></b></p>
<p>Punto de encuentro favorito: <b><%out.print(lugar);%></b></p>
</div>
<div style=position:relative;top:7rem;>
<h3>Nota media del usuario:</h3></br>
<p><b>La siguiente valoraci&oacute;n se trata de la valoraci&oacute;n actual del usuario establecida por nuestra comunidad de usuarios</b></p>
<p>Valoraci&oacute;n de la comunidad: <b><%out.print(" "+valoracion+" ");%></b></p>
</div></div>
<div class="col-2"></div>
</div>
</main>
<jsp:include page="footer2.jsp"/>
</body>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery.min.js"></script>
</html>