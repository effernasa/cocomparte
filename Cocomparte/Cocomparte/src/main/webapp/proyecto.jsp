<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Proyecto</title>
  <link rel="icon" href="img/logo.png" type="image/x-icon">

  <!-- Bootstrap core CSS -->
  <%-- s --%>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
      integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <link href="styles/estiloSobPro.css" rel="stylesheet">
</head>
<jsp:include page="header.jsp" />

<body>
  <br />
  <h1>Proyecto</h1>
  <p>Nuestro proyecto que estamos desarrollando se trata de un sitio web donde se puede interactuar a otros usuarios
    y cada a&ntilde;o, una tercera parte de la producci&oacute;n mundial de alimentos para consumo humano se pierde o
    desperdicia
    en la cadena de producci&oacute;n hasta que llega a la cocina de nuestros hogares. Esto significa 1.300 millones de
    toneladas anuales, suficientes para alimentar a 3.000 millones de personas...
    Aqui dejamos una gr&aacute;ficas donde
    nos indica qu&eacute; lugares y de qu&eacute; tipos de alimentos se desperdicia:
  </p>

  <br>
  <img src="img/grafica1.PNG" id="img1" />
  <br>
  <br>
  <img src="img/grafica2.PNG" id="img2" />
  <br>

  <p> Con este m&eacute;todo podemos, aparte de disminuir el porcentaje de la p&eacute;rdida de comida,
    cubrir las necesidades alimenticias de las familias que verdaderamente necesitan alimentos para poder dar de comer a
    sus hijos.
  </p>
  <p>A la vez podemos eliminar la pobreza de las familias con pocos recursos y acabar con el hambre que cada
    d&iacute;a el mundo est&aacute; generando.
  </p>

</body>
<jsp:include page="footer.jsp" />

</html>