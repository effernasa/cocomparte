<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@page import="java.sql.*,java.util.*"%>
<%@page import="com.cocomparte.*"%>
<link href="styles/productContainer.css" rel="stylesheet">
  <%
    myproductController product= new myproductController();
    String userId=(String) session.getAttribute("userid");
    Integer myuser = 0;
    myuser = Integer.parseInt(userId);
    System.out.println(myuser);
    product.insertarMisProductos(myuser);

    ArrayList<String> arrNomUsuario = new ArrayList<String>();
    arrNomUsuario = product.getArrNomUsuario();
    ArrayList<String> arrApellido = new ArrayList<String>();
    arrApellido = product.getArrApellido();
    ArrayList<Integer> arrIdProducto = new ArrayList<Integer>();
    arrIdProducto = product.getArrIdProducto();
    ArrayList<String> arrNomProducto = new ArrayList<String>();
    arrNomProducto = product.getArrNomProducto();
    ArrayList<String> arrOrigen = new ArrayList<String>();
    arrOrigen = product.getArrOrigen();
    ArrayList<String> arrDescripcion = new ArrayList<String>();
    arrDescripcion = product.getArrDescripcion();
    ArrayList<Integer> arrCantidad = new ArrayList<Integer>();
    arrCantidad = product.getArrCantidad();
    ArrayList<Integer> arrUnidades = new ArrayList<Integer>();
    arrUnidades = product.getArrUnidades();
    ArrayList<String> arrFecha = new ArrayList<String>();
    arrFecha = product.getArrFecha();
    ArrayList<Boolean> arrReservado = new ArrayList<Boolean>();
    arrReservado = product.getArrReservado();
    ArrayList<Boolean> arrPreparado = new ArrayList<Boolean>();
    arrPreparado = product.getArrPreparado();
    ArrayList<Integer> arrCategoriaId = new ArrayList<Integer>();
    arrCategoriaId = product.getArrCategoriaId();
    ArrayList<Integer> arrProveedorId = new ArrayList<Integer>();
    arrProveedorId = product.getArrProveedorId();
    ArrayList<String> arrFotoCategoria = new ArrayList<String>();
    arrFotoCategoria = product.getArrFotoCategoria();
    System.out.println(arrNomUsuario);
    System.out.println(arrApellido);
    System.out.println(arrOrigen);
    System.out.println(arrIdProducto);
    System.out.println(arrNomProducto);

    proveedor provedor = new proveedor();
    int prover = 0;
    int midda = arrIdProducto.size()-1;
    for (int i = 0;i<=midda;i++){
      prover = arrProveedorId.get(i);
      provedor.datosProveedor(prover);      
    }

    ArrayList<String> arrNomProveedor = new ArrayList<String>();
    arrNomProveedor = provedor.getArrNomProve();
    ArrayList<String> arrApellidos = new ArrayList<String>();
    arrApellidos = provedor.getArrApellidos();
    ArrayList<Integer> arrValoracion = new ArrayList<Integer>();
    arrValoracion = provedor.getArrValoracion();
    ArrayList<String> arrEmail = new ArrayList<String>();
    arrEmail = provedor.getArrEmail();
    ArrayList<String> arrEncuentro = new ArrayList<String>();
    arrEncuentro = provedor.getArrEncuentro();

    System.out.println(arrNomProveedor);
    System.out.println(arrApellidos);
    System.out.println(arrValoracion);
    System.out.println(arrEmail);
    System.out.println(arrEncuentro);

    int numProducto = arrIdProducto.size()-1;
    Double numProductos= new Double(numProducto);
    int contadorProducto = 0;
    long numFilas = Math.round(numProductos/3);

    if(((numProductos+1) % 3) == 0.0)
    {
      numFilas = Math.round(numProductos/3);
    }else{
      numFilas = Math.round(numProductos/3)+1;
    }
  System.out.println(numProductos);
  System.out.println(numFilas);
  System.out.println(Math.round(numProductos/3));


    //System.out.println(numFilas);
    //System.out.println(arrIdProductosPagina.get(contadorProducto));

    //for(int i = 1; i < numProductos;i++){
    //  System.out.println(arrProductosPagina[i]);
    //  System.out.println("Aqui entro");
    //}

  %>
  <div class="container marketing">
  <% 
  for(int j = 0; j < numFilas; j++){ 
  %>
      <div class="row linea flex flex-horizontal-center">
  <%
    for(int i = 1; i <= 3; i++){
      if(contadorProducto <= numProductos){%>
      <div class="col-lg cuadrado">
        <div class="col-lg me-auto"></div>
        <div class="col-lg me-auto producto" style="border: 0.2rem solid #276b3e; margin-top: 2rem; border-radius: 3rem; overflow: hidden;">
        <img style="object-fit: fill; position: relative; right: 15px; display: block;" src="<%out.println(arrFotoCategoria.get(arrCategoriaId.get(contadorProducto)));%>" width="110%" height="250px">
        <h2><%out.println(arrNomProducto.get(contadorProducto));%></h2>
        <p>Proveedor: <%out.print(arrNomProveedor.get(contadorProducto));%></p>
        <p>Fecha Cad: <%out.println(arrFecha.get(contadorProducto));%></p>
        <div class="row">
        <div class="col"></div>
        <div class ="col-4">
        <form name="formularioBoton" method="post" action="producto.jsp">
          <button type="text" name="idproducto" class="btn btn-secondary" value="<%out.println(arrIdProducto.get(contadorProducto));%>" style="margin-bottom: 5px;">M&aacute;s Info</button>
        </form>
        </div>
        <div class ="col"></div>
        <div class ="col-4">
        <form name="formularioBoton" method="post" action="proveedor.jsp">
          <button type="text" name="idproveedor" class="btn btn-secondary" value="<%out.println(arrProveedorId.get(contadorProducto));%>" style="margin-bottom: 5px;">Proveedor</button>
        </form>
        </div>
        <div class="col"></div>
        </div>
        </div><!--Column-->
        <div class= "col-lg me-auto"></div>
      </div> 
    <%  contadorProducto++;
       }
      } %>
    </div><!-- /.row0 -->
  <%
    }
  %>  
  </div>