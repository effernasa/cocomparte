<%@ page import="com.cocomparte.*" %>
<%@ page import ="java.util.*"%>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>CoComparte</title>
    <link rel="icon" href="img/logo.png" type="image/x-icon">

    <!-- Bootstrap core CSS -->
<%-- s --%>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="styles/carousel.css" rel="stylesheet">
  </head>
  <body>

<!--HEADER-->
<jsp:include page="header.jsp" />
    
<main role="main">
<div class="col-lg-12 text-light">
    <div id="client-testimonial-carousel"  class="carousel slide text-light" data-ride="carousel" style="background-color: #276b3e;" style="height:200px;">
      <div class="carousel-inner" role="listbox">
        <div class="carousel-item active text-center p-4">
            <img class="d-block w-25" src="img/img1.jpg" alt="First slide">
        </div>
        <div class="carousel-item text-center p-4">
            <img class="d-block w-50" src="img/img2.jpg" alt="Second slide">
        </div>
        <div class="carousel-item text-center p-4">
            <img class="d-block w-50" src="img/img3.jpg" alt="Third slide">
        </div>
      </div>
      <ol class="carousel-indicators">
        <li data-target="#client-testimonial-carousel" data-slide-to="0" class="active"></li>
        <li data-target="#client-testimonial-carousel" data-slide-to="1"></li>
        <li data-target="#client-testimonial-carousel" data-slide-to="2"></li>
      </ol>
    </div>
  </div>
  </br>
<!-- /.container -->
<jsp:include page="productContainer.jsp" />

  <!-- FOOTER -->
  <jsp:include page="footer.jsp" />
</main>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/popper.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <script src="https://code.jquery.com/jquery.min.js"></script>
</body>
</html>
