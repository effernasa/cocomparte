<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@page import="java.sql.*,java.util.*"%>
<%@ page import="com.cocomparte.*" %>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <title>Registrarse CoComparte</title>
    <link rel="icon" href="img/logo.png" type="image/x-icon">
    <!--Bootstrap core-->
    <!--<link href="assets/dist/css/bootstrap.min.css" rel="stylesheet">-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">


    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="styles/login.css" rel="stylesheet">
</head>
<%

ArrayList<String> ciudad = BD.visualizarCiudades();


/*int j = 0;
while(ciudades.next()){
  ciudad.add(ciudades.getString("Nombre"));
  System.out.println(ciudades.getString("Nombre"));
  j++;
} */
int mida = ciudad.size();

/*ArrayList<Object> ciudad= conexionBD.visualizarCiudades();*/
//String[] ciudad= BD.visualizarCiudades();
%>
<body class="text-center">  
    <form class="form-login" method="post" action="registrarusuarioBD.jsp">
      <h1><i>Registro de usuario</i></h1></br>
        <div class="form-row">
          <div class="col-md-6 form-group">
            <label for="inputName">Nombre</label>
            <input type="text" name="nombre" class="form-control" id="inputName" placeholder="Inserte su nombre" required>
          </div>
          <div class="form-group col-md-6 c2">
            <label for="inputSurname">Apellidos</label>
            <input type="text" name="apellidos" class="form-control" id="inputSurname" placeholder="Inserte sus apellidos">
          </div>
        </div>
        <div class="form-row">
          <div class="form-group col-md-6 col-12">
            <label for="inputDNI">D.N.I/N.I.E</label>
            <input type="text" name="dni" class="form-control" id="inputDNI" placeholder="Inserte su DNI/NIE" required>
          </div>
          <div class="form-group col-md-6 col-12 c2">
            <label for="inputCity">Email</label>
            <input type="text" name="email" class="form-control" id="inputEmail" placeholder="Inserte su Email" required>
          </div>
        </div>
        <div class="form-row">
        <div class="form-group col-md-6 col-12">
          <label for="inputCity">Ciudad</label>
          <select id="inputCity" class="form-control" name="ciudad">
                        <%//int mida=ciudad.length;
                        for(int i=0;i<=mida-1;i++){
                        %>
                        <option value="<%out.print(ciudad.get(i));%>"><%out.print(ciudad.get(i));%></option>
                        <%
                        //out.println("<option value="+ciudad.get(i)+">"+ciudad.get(i)+"</option>");
                        }
                        %>
                </select>
          <%--<input list="citys" class="form-control" id="inputCity" placeholder="Seleccione su ciudad" required>
          <datalist id="citys" name="citys" multiple>
            <%for(int i=0;i<=ciudad.length();i++){
              out.println("<option value="+ciudad[i]+">"+ciudad[i]+"</option>")
            }
            
            %>
          </datalist>--%>
        </div>
        <div class="col-sm"></div>
        <div class="form-group col-md-6 col-12 c2">
          <label for="inputMobile">M&oacute;vil</label>
          <input type="text" name="mobile" class="form-control" id="inputMobile" placeholder="Inserte su n&uacute;mero de m&oacute;vil" required>
        </div></div> <!--Final de la tercera linea de cuestionario-->
        <div class="form-row">
          <div class="col-md-12 col-12 cdirec">
          <label for="inputStreet">Direcci&oacute;n</label>
        </div></div>
        <div class ="form-row">
          <div class = "col-md-12 col-12 cdirec">
          <input type="text" name="direccion" class="form-control" id="inputStreet" placeholder="Inserte su direcci&oacute;n">
        </div></div>
        <div class="form-row">
          <div class="form-group col-md-6 col-12">
            <label for="inputPassword">Contrase&ntilde;a</label>
            <input type="password" name="password" class="form-control" id="inputPassword" placeholder="Contrase&ntilde;a" required>
          </div>
          <div class="form-group col-md-6 col-12 form-check espe c2" style=position:relative;padding-left:0rem; id="esper" >
            <label for ="prove">Proveedor</label>
            <select name="check" class="form-control" id="prove">
              <option value="1">Si</option>
              <option value="2">No</option>
            </select>
          </div>
        </div>
            </select>
          </div>
        </div></br>
        <div class="form-row">
          <div class="form-group col-md-12">
          <button type="submit" class="btn btn-primary fin">Registrarse</button>
          </div>
      </div>
      </form>
</body>
</html>