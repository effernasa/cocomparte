<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Sobre nosotros</title>
    <link rel="icon" href="img/logo.png" type="image/x-icon">

    <!-- Bootstrap core CSS -->
    <%-- s --%>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
            integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">

        <style>
            .bd-placeholder-img {
                font-size: 1.125rem;
                text-anchor: middle;
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
            }

            @media (min-width: 768px) {
                .bd-placeholder-img-lg {
                    font-size: 3.5rem;
                }
            }
        </style>
        <link href="styles/estiloSobPro.css" rel="stylesheet">
</head>
<jsp:include page="header.jsp" />

<body>
    <br />
    <h1>Sobre nosotros</h1>
    <p>Somos un grupo de alumnos de la clase Java en la entidad Fundaci&oacute; Esplai.</p>
    <p>Las funciones que realizamos y repartimos son las siguientes: </p>
    <p><b>&middot; Efr&eacute;n: </b>Hizo el pap&eacute;l como de cliente por si le gustaba las funciones de nuestra
        p&aacute;gina y si estaba de acuerdo con la estructura y el dise&ntilde;o de la p&aacute;gina. Realiz&oacute; la
        documentaci&oacute;n del proyecto, el diagrama de la planificaci&oacute;n de
        nuestro proyecto y los formularios para crear una cuenta, a&ntilde;adir un producto e iniciar una sesi&oacute;n
        una vez de que el cliente de haya registrado.
    </p>
    <p>Durante el proceso del proyecto pidi&oacute; la ayuda tanto de sus
        compa&ntilde;eros como del profesor para ense&ntilde;ar los m&eacute;todos que estaban haciendo o sacar de dudas
        de alg&uacute;n problema, adquiri&oacute; much&iacute;ssimo conocimiento.
    </p>
    <p><b>&middot; Marc: </b>Se encarg&oacute; de la parte de front-end para darle el dise&ntilde;o mediante css y
        BootStrap
        con los archivos jsp agregando animaciones como el carousel. Tambi&eacute;n hizo la parte de back-end para sacar
        la informaci&oacute;n de una tabla en
        la base de datos y sacarlas para el usuario lo pueda ver con Java. Dise&ntilde; el logo de nuestra p&aacute;gina
        y aplicarlo tanto en la web como en las pesta&ntilde;as en cualquier navegador.</p>
    <p><b>&middot; Octavio: </b>Se encarg&oacute; de la base de datos, con Efr&eacute;n juntos hicieron las tablas
        necesarias y la diagrama.
        Despu&eacute;s realiz&oacute; los m&eacute;todos con las querys necesarios para a&ntilde;adir informaci&oacute;n
        en las tablas, autorellenar algunas tablas para los formularios y sacar las datos de un usuario y los productos
        que ha subido. Tambi&eacute;n colabor&oacute; en algunas ayudas y mejoras tanto en la parte de front-end y
        back-end.</p>

</body>
<jsp:include page="footer.jsp" />

</html>