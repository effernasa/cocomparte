<%@ page import="com.cocomparte.*" %>
<%@ page import ="java.util.*"%>
<%@ page import ="java.sql.*"%>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>CoComparte</title>
    <link rel="icon" href="img/logo.png" type="image/x-icon">

    <!-- Bootstrap core CSS -->
<%-- s --%>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="styles/carousel.css" rel="stylesheet">
  </head>

  <body class="text-center">

<!--HEADER-->
<jsp:include page="header.jsp" /></br>
    
<main role="main"><p></br></p>
<h1>Mis pedidos actuales</h1></br>
<!-- /.container -->
<jsp:include page="myproductContainer.jsp" />

  <!-- FOOTER -->
  <jsp:include page="footer.jsp" />
</main>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/popper.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <script src="https://code.jquery.com/jquery.min.js"></script>
</body>
</html>