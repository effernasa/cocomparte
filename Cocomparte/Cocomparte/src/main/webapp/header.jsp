<%@ page import="com.cocomparte.*" %>
<%@ page import="java.util.*"%>
<%@ page import="java.util.Enumeration"%>
<link href="styles/header.css" rel="stylesheet">
<header>
  <nav class="navbar navbar-expand-md navbar-dark fixed-top" style="background-color: #276b3e;">
  <img class="navbar-brand" src="img/logo.png" alt="LogoCoComparte" width="50px" height="50px" style="object-fit: fill; position: relative; left: 10px;">
    <a class="navbar-brand">CoComparte</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="index.jsp">Inicio</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="proyecto.jsp">Proyecto</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="sobrenosotros.jsp">Sobre Nosotros</a>
        </li>
      </ul>
      
      <div class="collapse navbar-collapse justify-content-end" id="LRButtons">

      <%

        //Session session = request.getSession();
        Enumeration attributeNames = session.getAttributeNames();
        while(attributeNames.hasMoreElements()){
          String name = (String) attributeNames.nextElement();
          String value = (String) session.getAttribute(name);
          System.out.println(name + "=" + value);
        }

       if(session.getAttribute("userid") != null){
          String userId=(String) session.getAttribute("userid");
          String userName = BD.Nombre(Integer.parseInt(userId));
            out.println("<a class='btn btn-outline-success float-right my-2 my-sm-0'id='LRButtons' href='logout.jsp'>Sign Out</a>");
            //out.println("<button class='btn btn-outline-success float-right my-2 my-sm-0'id='LRButtons'>"+ out.print(userName.substring(0, 1).toUpperCase() + userName.substring(1))+"</button>");
      %>
            
      <div class="btn-group dropleft">
        <button type="button" class="btn btn-outline-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <%out.print("@"+userName.substring(0, 1).toUpperCase() + userName.substring(1));%>
        </button>
          <div class="dropdown-menu" style="right:20px;">
            <a class="dropdown-item" href="infoUsuario.jsp">Informacion Usuario</a>
            <%
            if(session.getAttribute("proveedorid") != null){
              out.println("<a class='dropdown-item' href='agregarProducto.jsp'>Agregar Producto</a>");
            }
            %>
            <a class="dropdown-item" href="misProductos.jsp">Mis pedidos</a>
          </div>
      </div>
      <%
      }else{
        %>
        
        <a class="btn btn-outline-success float-right my-2 my-sm-0" id="LRButtons" href="register.jsp">Register</a>
        <a class="btn btn-outline-success float-right my-2 my-sm-0" id="LRButtons" href="login.jsp">Log in</a>
      
        <%
      }
        %>

      </div>
    </div>
  </nav>
</header>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
