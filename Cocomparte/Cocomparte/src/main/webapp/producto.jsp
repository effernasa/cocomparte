<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@page import="java.sql.*,java.util.*"%>
<%@page import="com.cocomparte.*"%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Solicitar producto</title>
    <link rel="icon" href="img/logo.png" type="image/x-icon">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
      integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <link href="styles/producto.css" rel="stylesheet">
</head>
<body>
    
<!--HEADER-->
<jsp:include page="header.jsp" />

<%
  String idproducto=request.getParameter("idproducto");
  System.out.println(idproducto);
  String nombre = "";
  String origen = "";
  String categoria = "";
  int cantidad = 1;
  int unidades = 1;
  String fechaCad = "";
  boolean reservado = false;
  boolean preparado = false;
  String image=request.getParameter("image");

  ResultSet infoProducto = BD.consulta("SELECT productos.*, categoria.Categoria FROM productos INNER JOIN Categoria ON Categoria_idCategoria = categoria.idCategoria WHERE id ="+ idproducto +";");
  while(infoProducto.next()){
    nombre = infoProducto.getString("Nombre");
    origen = infoProducto.getString("Origen");
    categoria = infoProducto.getString("Categoria");
    cantidad = infoProducto.getInt("Cantidad");
    unidades = infoProducto.getInt("Unidades");
    fechaCad = infoProducto.getString("FechaCaducidad");
    if(infoProducto.getInt("Reservado")==1){
      reservado = true;
    }
    if(infoProducto.getInt("Preparado")==1){
      reservado = true;
    }
  }
%>

<div class="row" style="margin: 5rem;">
  <div class="span8">
    <div class="row">
      <div class="span8">
        <h4><strong><%out.println(nombre);%></strong></h4>
      </div>
    </div>
    <div class="row">
        <div class="col-4">
            <img src="<%out.println(image);%>" style=max-width:330px;max-height:250px; alt="">
        </div>
        <div class="col-8">
            <p>
                <%out.println(origen);%>
            </p>
            <p>
                <i class="icon-user"></i> Este producto lo ha subido <a href="#">**ID USUARIO PROVEDOR**</a> 
            </p>
        </div>
  </div>
      <div class="row">
        <div class="col-4 mx-auto">
            <h2>Cola:</h2>**Num personas por encima tuyo a la hora de pedir el producto**
        </div>
        <div class="col-8">
        <form method="post" action="agregarUsuarioProducto.jsp">
            <input type="hidden" value="<%out.println(idproducto);%>" name="productoID">
            <button type="submit" class="btn btn-primary">Solicitar</button>
        </form>
        </div>
  </div>
</div>

<!-- FOOTER -->
<jsp:include page="footer.jsp" />
</body>
</html>