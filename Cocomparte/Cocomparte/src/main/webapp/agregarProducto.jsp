<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@page import="java.sql.*,java.util.*"%>
<%@ page import="com.cocomparte.*" %>
<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <title>A&ntilde;adir producto</title>
    <!--Bootstrap core-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">

    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
    </style>
    <!-- Custom styles for this template -->
    <link href="styles/agregarProd.css" rel="stylesheet">
</head><%
ArrayList<String> categoria = BD.visualizarCategoria();
ArrayList<String> categorias= new ArrayList<String>();


/*int j = 0;
while(ciudades.next()){
  ciudad.add(ciudades.getString("Nombre"));
  System.out.println(ciudades.getString("Nombre"));
  j++;
} */
int mida = categoria.size();
%>
<!--
    nombre: string /
    origen: string /
    cantidad: int /
    unidades: int /
    fecha de caducidad: date
    reservado: checkbox
    proveedor: string; para la bdd sacar la id del nombre que ha puesto
    categoria: desplegable; para la bdd sacar la id de la opcion que ha seleccionado
-->

<body class="text-center">
    <form class="form-login" method="post" action="agregarProductoBD.jsp">
        <h1><i>Nuevo producto</i></h1></br>
        <div class="form-row">
            <div class="col-md-6 col-12 form-group">
                <label for="inputName">Nombre</label>
                <input type="text" class="form-control" name="inputName" placeholder="Nombre" required>
            </div>
            <div class="form-group col-md-6 col-12 c2">
                <label for="inputOrigen">Origen</label>
                <input type="text" class="form-control" name="inputOrigen" placeholder="Origen">
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6 col-12">
                <label for="inputCantidad">Cantidad</label>
                <input type="number" min=0 class="form-control" name="inputCantidad" placeholder="Cantidad del producto"
                    required>
            </div>
            <div class="form-group col-md-6 col-12 c2">
                <label for="inputUnidad">Unidad</label>
                <input type="number" min=0 class="form-control" name="inputUnidad" placeholder="Unidades del producto" required>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6 col-12">
                <label for="inputFechaCad">F. Caducidad</label>
                <input type="text" class="form-control" name="inputFechaCad" placeholder="YYYY-MM-DD" required>
            </div>
            <div class="form-group col-md-6 col-12 c2">
                <label for="inputCategory">Categoria</label>
                <select name="inputCategory" class="form-control">
                        <%//int mida=ciudad.length;
                        for(int i=0;i<=mida-1;i++){
                        %>
                        <option value="<%out.print(categoria.get(i));%>"><%out.print(categoria.get(i));%></option>
                        <%
                        //out.println("<option value="+ciudad.get(i)+">"+ciudad.get(i)+"</option>");
                        }
                        %>
                </select>
            </div>
        </div>
        <div class="form-row form-group">
            <div class="form-group col-md-6 col-12 form-check espe" style=position:relative;padding-left:0rem; id="esper" >
            <label for ="prove">Preparado</label>
            <select name="check" class="form-control">
              <option value="1">Si</option>
              <option value="2">No</option>
            </select>
          </div>
            <div class="form-group col-md-6 col-12" style=position:relative;top:0rem;>
            </br>
                <button type="submit" class="btn btn-primary fin">A&ntilde;adir producto</button>
            </div>
        </div>
    </form>
</body>

</html>