<div id="carouselExampleControls" class="carousel slide" data-ride="carousel" style="background-color: #276b3e;">
    <img src="img/logo.png" >
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img class="d-block" src="img/img1.jpg" alt="First slide">
      </div>
      <div class="carousel-item">
        <img class="d-block" src="img/img2.jpg" alt="Second slide">
      </div>
      <div class="carousel-item">
        <img class="d-block" src="img/img3.jpg" alt="Third slide">
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>