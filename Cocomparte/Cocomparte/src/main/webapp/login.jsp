<%@ page import="com.cocomparte.*" %>
<!doctype html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="google-signin-scope" content="profile email">
    <meta name="google-signin-client_id" content="YOUR_CLIENT_ID.apps.googleusercontent.com">
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <title>Login CoComparte</title>
    <link rel="icon" href="img/logo.png" type="image/x-icon">

    <!-- Bootstrap core CSS -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="styles/register.css" rel="stylesheet">
  </head>
  <body class="text-center">
    <form class="form-signin" style="background-color: #2d2f2d; border-radius: 10px;" method="post" action="process.jsp">
  <img class="mb-4" src="img/logo.png" alt="" width="72" height="72">
  <h1 class="h3 mb-3 font-weight-normal ">Login</h1>
  
  <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email" required autofocus>
  
  <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Constrase&ntilde;a" required>
  <div class="checkbox mb-3">
    <label>
      <input type="checkbox" value="remember-me"> Recordarme
    </label>
  </div>
  <div class="login">
  <button class="btn btn-lg btn-info btn-block" style="font-size:16px" type="submit">Log in</button>
  </div>
  <a class="btn btn-outline-light mt-2 btn-block" href="/users/googleauth" role="button" style="text-transform:none">
    <img width="20px" style="margin-bottom:3px; margin-right:5px" alt="Google sign-in" src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Google_%22G%22_Logo.svg/512px-Google_%22G%22_Logo.svg.png" />
    Login with Google
  </a>
  <p class="mt-5 mb-3 text-muted">&copy; 2021 M.O.E Tech, Inc. &middot;</p>
</form>

<script>
  function onSignIn(googleUser) {
    // Useful data for your client-side scripts:
    var profile = googleUser.getBasicProfile();
    console.log("ID: " + profile.getId()); // Don't send this directly to your server!
    console.log('Full Name: ' + profile.getName());
    console.log('Given Name: ' + profile.getGivenName());
    console.log('Family Name: ' + profile.getFamilyName());
    console.log("Image URL: " + profile.getImageUrl());
    console.log("Email: " + profile.getEmail());

    // The ID token you need to pass to your backend:
    var id_token = googleUser.getAuthResponse().id_token;
    console.log("ID Token: " + id_token);
  }
</script>
</body>
</html>
