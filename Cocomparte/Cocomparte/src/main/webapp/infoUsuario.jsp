<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@page import="java.sql.*,java.util.*"%>
<%@page import="com.cocomparte.*"%>
<%@page import="com.cocomparte.*"%>
<%@page import="java.util.ArrayList"%>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <title>Informaci&oacute;n del usuario</title>
    <!--Bootstrap core-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">

    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
    </style>
    <!-- Custom styles for this template -->
    <link href="styles/infoUsuario.css" rel="stylesheet">
    <link rel="icon" href="img/logo.png" type="image/x-icon">
</head>
<% 
    String userId=(String) session.getAttribute("userid");
    ResultSet rs = BD.infoUsuario(Integer.parseInt(userId));
    String nombre = null;
    String apellido = null;
    String dni = null;
    int telefono = 0;
    String email = null;
    String direccion = null;
    String ciudad = null;
    while(rs.next()){
        nombre = rs.getString("u.Nombre");
        apellido = rs.getString("u.Apellidos");
        dni = rs.getString("u.DNI");
        telefono = rs.getInt("u.Telefono");
        email = rs.getString("u.Email");
        direccion = rs.getString("u.Direccion");
        ciudad = rs.getString("c.Nombre");
    }
    
%>
<body class="text-center">
    <div class="panel-group">
        <fieldset>
            <div class="panel panel-default">
                <legend>
                    <div class="panel-heading">
                        <h1>Datos personales</h1>
                    </div>
                </legend>
                <div class="panel-body">
                    <p><b>Nombre:</b> <%out.println(nombre);%> <b>|</b>
                        <b>Apellido:</b> <%out.println(apellido);%> <b>|</b>
                        <b>DNI:</b> <%out.println(dni);%> <b>|</b>
                    </p>
                    <p><b>Telefono:</b> <%out.println(telefono);%> <b>|</b>
                        <b>Email:</b> <%out.println(email);%> <b>|</b>
                    </p>
                    <p><b>Direcci&oacute;n:</b> <%out.println(direccion);%> <b>|</b></p>
                    <p><b>Ciudad:</b> <%out.println(ciudad);%> </p>
                </div>
            </div>
        </fieldset>
        <br />
        <fieldset>
            <div class="panel panel-default">
                <legend>
                    <div class="panel-heading">
                        <h1>Productos subidos</h1>
                    </div>
                </legend>
                <%
                
                    /*productController product= new productController();
                    product.insertarProductosEnArray();

                    ArrayList<String> arrNombre = new ArrayList<String>();
                    arrNombre = product.getArrProductos();
                    ArrayList<String> arrOrigen= new ArrayList<String>();
                    arrOrigen = product.getArrOrigen();
                    ArrayList<Integer> arrCantidad = new ArrayList<Integer>();
                    arrCantidad = product.getArrCantidad();
                    ArrayList<Integer> arrUnidades = new ArrayList<Integer>();
                    arrUnidades = product.getArrUnidades();
                    ArrayList<Boolean> arrPreparado = new ArrayList<Boolean>();
                    arrPreparado = product.getArrPreparado();
                    ArrayList<String> arrCategoria = new ArrayList<String>();
                    arrCategoria = product.getArrStringCategoria();*/

                    ResultSet rsp = BD.infoProducto(Integer.parseInt(userId));
                    ArrayList<String> nom = new ArrayList<String>();
                    ArrayList<String> origen = new ArrayList<String>();
                    ArrayList<Integer> cantidad = new ArrayList<Integer>();
                    ArrayList<Integer> unidades = new ArrayList<Integer>();
                    ArrayList<String> fechaCaducidad = new ArrayList<String>();
                    ArrayList<Boolean> preparado = new ArrayList<Boolean>();
                    ArrayList<String> categoria = new ArrayList<String>();

                    while(rsp.next()){

                        nom.add(rsp.getString("p.Nombre"));
                        origen.add(rsp.getString("p.Origen"));
                        cantidad.add(rsp.getInt("p.Cantidad"));
                        unidades.add(rsp.getInt("p.Unidades"));
                        fechaCaducidad.add(rsp.getString("p.FechaCaducidad"));
                        preparado.add(rsp.getBoolean("p.Preparado"));
                        categoria.add(rsp.getString("c.Categoria"));

                    }

                    int numProductos = nom.size()-1;

                
                %>
                <div class="panel-body">
                <%
                    for(int i = 0; i <= numProductos; i++){%>
                    <p style="font-weight: bold; text-decoration: underline;">Producto <%out.print(i + 1);%></p>
                    <p><b>Nombre:</b> <%out.println(nom.get(i));%> <b>|</b> <b>Origen:</b> <%out.println(origen.get(i));%> <b>|</b> </p>
                    <p><b>Cantidad:</b> <%out.println(cantidad.get(i));%> <b>|</b> <b>Unidades:</b> <%out.println(unidades.get(i));%> <b>|</b> <b>Fecha de caducidad:</b> <%out.println(fechaCaducidad.get(i));%> <b>|</b> </p>
                    <p><b>Preparado:</b> <%if(preparado.get(i) == false){out.println("No");}else{out.println("Si");}%> <b>|</b> <b>Categor&iacute;a:</b> <%out.println(categoria.get(i));%> </p><br/>
                    <%}%>
                    <hr>
                </div>
            </div>
        </fieldset>
        <button type="button" class="btn btn-outline-success"><a class="nav-link" href="index.jsp"  style="color: white">Volver a la p&aacute;gina de inicio</a></button>
    </div>
</body>

</html>